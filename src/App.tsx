import React from 'react';
import './App.css';
import RootNavigation from './components/RootNavigation';

function App() {
  return (
     <RootNavigation/>
  );
}

export default App;
