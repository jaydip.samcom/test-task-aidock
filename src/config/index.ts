import Home from "../pages/Home";

export const routes: any = [
  {
    key: "home",
    title: "Home",
    tooltip: "Home",
    path: "/",
    enabled: true,
    component: Home,
  },
];
