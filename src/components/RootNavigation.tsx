import React, { ReactElement, FC } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { routes } from "../config";

// default component
const DefaultComponent: FC<{}> = (): ReactElement => (
  <div>{`No Component Defined.`}</div>
);

const RootNavigation: FC<{}> = (): ReactElement => {
  return (
    <>
      <BrowserRouter>
        <Routes>
          {routes
            ? routes.map((item: any, i: any) => (
                <Route
                  key={i}
                  path={`${item.path}`}
                  element={<item.component /> || <DefaultComponent />}
                  //exact
                />
              ))
            : null}
        </Routes>
      </BrowserRouter>
    </>
  );
};

export default RootNavigation;
