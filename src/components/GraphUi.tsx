import React, { FC, ReactElement } from "react";
import { Graph } from "react-d3-graph";

interface Props {
    graphData: any;
    graphConfig: any;
    onClickNode:any;
    onClickLink:any;
}

const GraphUi: FC<Props> = ({ graphData, graphConfig,onClickNode,onClickLink }: Props): ReactElement => {
    return (
        <>
         {graphData && (
            <Graph
              id="graph-id" // id is mandatory
              data={graphData}
              config={graphConfig}
              onClickNode={onClickNode}
              onClickLink={onClickLink}
            />
          )}
        </>
    )
}



export default GraphUi;
